import time
import boto3
import uuid
import logging

from botocore import config
from urllib.request import urlretrieve
from collections import namedtuple
from os import getenv, path

import cv2 as cv
import numpy as np

'''
I'm too lazy to create project and start it instead of start file.py so... :)
logger.py
'''

def get_module_logger(mod_name):
    """
    To use this, do logger = get_module_logger(__name__)
    """
    logger = logging.getLogger(mod_name)
    handle = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
    handle.setFormatter(formatter)
    logger.addHandler(handle)
    logger.setLevel(logging.DEBUG)
    return logger


''''''


TMP_PATH = '/tmp/'
MODEL = "feathers.t7"

logger = get_module_logger(__name__)
Config = namedtuple('Config', ['name', 'URL', 'input', 'output'])
S3Config = Config('s3', 'https://storage.yandexcloud.net', 'alexelex-input', 'alexelex-output')
S3RegionConfig = config.Config(region_name='ru-central1')


def predict(net, img, h, w):
    blob = cv.dnn.blobFromImage(img, 1.0, (w, h),
                                (103.939, 116.779, 123.680), swapRB=False, crop=False)

    logger.info("Setting the input to the model")
    net.setInput(blob)

    logger.info("Starting Inference!")
    start = time.time()
    out = net.forward()
    end = time.time()
    logger.info("Inference Completed successfully!")

    # Reshape the output tensor and add back in the mean subtraction, and
    # then swap the channel ordering
    out = out.reshape((3, out.shape[2], out.shape[3]))
    out[0] += 103.939
    out[1] += 116.779
    out[2] += 123.680
    out /= 255.0
    out = out.transpose(1, 2, 0)

    # Printing the inference time
    logger.info("The model ran in {:.4f} seconds".format(end - start))
    return out


def resize_img(img, width=None, height=None, inter=cv.INTER_AREA):
    dim = None
    h, w = img.shape[:2]

    if width is None and height is None:
        return img
    elif width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    elif height is None:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv.resize(img, dim, interpolation=inter)
    return resized


def process_image(image, model):
    logger.info("start process_image()")
    net = cv.dnn.readNetFromTorch(model)
    img = cv.imdecode(np.asarray(bytearray(image)), cv.IMREAD_COLOR)
    img = resize_img(img, width=600)
    h, w = img.shape[:2]
    out = predict(net, img, h, w)
    out = cv.convertScaleAbs(out, alpha=255.0)
    return cv.imencode('.jpg', out)[1].tostring()


def load_model():
    logger.info("loading model")
    model_path = path.join(TMP_PATH, MODEL)
    url = 'https://gitlab.com/sol/style-transfer-seed/-/raw/master/models/' + MODEL_FILE
    urlretrieve(url, model_path)
    return model_path


def handler(event, context):
    session = boto3.session.Session()
    s3 = session.client(
        service_name=S3Config.name,
        endpoint_url=S3Config.URL,
        aws_access_key_id=getenv('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=getenv('AWS_SECRET_ACCESS_KEY'),
        config=S3RegionConfig
    )

    object_id = event['messages'][0]['details']['object_id']
    logger.info(f"object_id = {object_id}")

    get_object_response = s3.get_object(Bucket=Config.input, Key=object_id)
    input_image = get_object_response['Body'].read()
    output_image = process_image(input_image, load_model())

    logger.info("loading to storage")
    s3.put_object(Bucket=Config.output, Key=str(uuid.uuid4()), Body=output_image)
    logger.info("loaded to storage")